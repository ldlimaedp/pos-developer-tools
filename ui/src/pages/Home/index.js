import React, { Component, Fragment } from 'react';
import Header from '../../components/Header'
import NavMenu from '../../components/NavMenu'
import Dashboard from '../../components/Dashboard'

class Home extends Component {
  constructor(props) {
    super(props)
    this.state = {}
  }

  render() {
    return (
      <Fragment>
        <Header>
          <NavMenu user="@admin" />
        </Header>
        <div className="container">
          <Dashboard position="centro">
          </Dashboard>
        </div>
      </Fragment>
    );
  }
}

export default Home;
